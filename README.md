# User manager Tool - partiel

***

### Contenu du dépot

- Le fichier workbench `partiel_model.mwb` contenant la modélisation de base de données.
- -Le dossier de développement `vacherot_partiel_developpement`.

### Information pratiques

Dans un souci de sécurité, bien que non demandé explicitement dans la consigne, un espace admin a été mis en place pour la gestion des utilisateurs dans la mesure où il s'agit de manipulation de données.

- Informations d'authentifications
  - `Mail` - admin@admin.com
  - `Password` - x251rt


### Information de livraison

- `adresse test server` - http://vacherot.etudiant-eemi.com/perso/dossier/malcolm0810/vacherot_partiel_2/index.php


### Description des fichiers de l'architecture

```
api-v1/
  |- app/
  |  |- controllers/
  |  |  |- <Classes définissant l'affichage d'une page, ses données et permettant de gérér son aspect fonctionnel>
  |  |- models/
  |  |  |- <models de données>
  |  |- services/
  |  |  |- <composants réutilisables>
  |  |  |- <Hash>
  |  |  |- <Paginate>
  |  |  |- <SendMail>
  |  |  |- <Session>
  |  |  |- <UploadFile>
  |  |  |- <Validator>
  |  |- vendors/
  |  |  |- <less>
  |  |  |- <twig>
  |  |  |- <yaml>
  |- configuration/
  |  |- admin/
  |  |  |- <espaces admin et pages respectivement associées>
  |  |- database/
  |  |  |- <fichiers de configuration pour accès bdd selon environnement de dev>
  |  |- design files/
  |  |  |- <fichiers permettant de gérer les dépendances pour fichiers css, js, less, bootstrap en fonction des pages>
  |  |  |- <config_bootstrap>
  |  |  |- <config_jsfile>
  |  |  |- <config_less>
  |  |  |- <config_stylesheet>
  |  |- environnement/
  |  |  |- <definition environnment de dev>
  |  |- twig/
  |  |  |- <config_templates>
  |  |  |- <config_twig>
  |- core/
  |  |- database/
  |  |  |- <Database - fichier de connexion à la base de données>
  |  |  |- <Models - query builders utilisés par les fichiers models>
  |  |- system/
  |  |  |- <fichiers permettant le fonctionnement du Model Controller>
  |  |  |- <AbstractPageSystem>
  |  |  |- <PageSystem>
  |  |  |- <System>
  |  |- Autoloader
  |  |- Configuration
  |- lang/
  |  |- en/
  |  |  |- <fichier clés de langue en anglais>
  |  |- fr/
  |  |  |- <fichier clés de langue en français>
  |- templates/
  |  |- mail_tempaltes/
  |  |  |- <fichiers de templates mail>
  |- views/
  |  |- macros/
  |  |  |- <fichiers contenant les macros twig>
  |  |  |- <bootstrapForm>
  |  |  |- <form>
  |  |- pages/
  |  |  |- <pages du site>
  |  |- <fichiers layout>
  |- www/
  |  |- ajax/
  |  |  |- <index ajax>
  |  |- assets/
  |  |  |- bootstrap/
  |  |  |- css/
  |  |  |- fonts/
  |  |  |- js/
  |  |  |- less/
  |  |  |- pictures/
  |- index
```

=======
