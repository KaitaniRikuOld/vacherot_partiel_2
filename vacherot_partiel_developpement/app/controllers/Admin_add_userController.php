<?php

/**
 * Class HomeController
 *
 * Fichier de vue inclue: home
 * Ce fichier étends la class AbstractPageSystem où sont accessibles:
 * Les super globales: $this->get, $this->get, $this->get, $this->get, $this->get
 *
 * @author : Kévin Vacherot
 *
 */

namespace App\Controllers;

class Admin_add_userController extends \Core\System\AbstractPageSystem
{
    private $user;

    /**
     * __Constructeur: C'est dans le constructeur que nous définissons les informations de la page courante
     * @return void
     */
    public function __construct()
    {
        $this->setPageView('admin_add_user.twig');
        $this->user = new \App\Models\User();
    }

    /**
     * Méthode appelée systématiquement par le controller courant depuis la classe PageSystem
     * C'est depuis cette méthode que seront gérées:
     * L'ensemble des données revoyées vers la vue (select de données par exemples)
     * L'ensemble des traitement générés depuis la vue (envoi de formulaire par exemple)
     *
     * Les traitements pourront évidemment être effectués depuis d'autres méthodes de la classe : traiter des post et retourner des données
     * Mais ils devront être exécutés depuis main()
     *
     * @return void
     */
    public function main()
    {
        $this->setPageInfos(array(
            'page_title' => isset($this->get['modif_user']) ? "Modifier users" : "Ajouter user",
        ));

        if(isset($this->post['creer']) || isset($this->post['modifier'])){
            if(isset($this->post['creer'])){
                $this->createUser($this->post);
            } else if(isset($this->post['modifier'])){
                $this->updateArticle($this->get['modif_user'], $this->post);
            }
        }

        $this->setVariablesToView(array(
            'user' => isset($this->get['modif_user']) ? $this->user->getUsers(array('user_id' => $this->get['modif_user'])) : null,
            'titre_module' => isset($this->get['modif_user']) ? 'Modification d\'un user' : 'Ajouter un user',
            'submit' => isset($this->get['modif_user']) ? 'modifier' : 'creer',
        ));
    }

    private function createUser($form_post)
    {
        $user_pseudo = $form_post['pseudo'];
        $user_mail = $form_post['mail'];
        $user_password = md5($form_post['password']);
        $category_id = $form_post['category_id'];

        if($this->user->insertUser($user_pseudo, $user_mail, $user_password, $category_id)){
            $this->redirect('admin_add_user', array('created' => 'success'));
        } else {
            $this->redirect('admin_add_user', array('created' => 'failed'));
        }
    }

    private function updateArticle($user_id, $form_post)
    {
        $user_pseudo = $form_post['pseudo'];
        $user_mail = $form_post['mail'];
        $category_id = $form_post['category_id'];

        if($this->user->updateUser($user_id, $user_pseudo, $user_mail, $category_id)){
            $this->redirect('admin_gestion_users', array('updated' => 'success'));
        } else {
            $this->redirect('admin_gestion_users', array('updated' => 'success'));
        }
    }
}
