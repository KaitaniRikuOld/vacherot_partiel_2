<?php

/**
 * Class HomeController
 *
 * Fichier de vue inclue: home
 * Ce fichier étends la class AbstractPageSystem où sont accessibles:
 * Les super globales: $this->get, $this->get, $this->get, $this->get, $this->get
 *
 * @author : Kévin Vacherot
 *
 */

namespace App\Controllers;

class Admin_auditController extends \Core\System\AbstractPageSystem
{
    private $user;
    /**
     * __Constructeur: C'est dans le constructeur que nous définissons les informations de la page courante
     * @return void
     */
    public function __construct()
    {
        $this->setPageView('admin_audit.twig');
        $this->setPageInfos(array(
            'page_title' => 'Actions logs'
        ));
        $this->user = new \App\Models\User();
    }

    /**
     * Méthode appelée systématiquement par le controller courant depuis la classe PageSystem
     * C'est depuis cette méthode que seront gérées:
     * L'ensemble des données revoyées vers la vue (select de données par exemples)
     * L'ensemble des traitement générés depuis la vue (envoi de formulaire par exemple)
     *
     * Les traitements pourront évidemment être effectués depuis d'autres méthodes de la classe : traiter des post et retourner des données
     * Mais ils devront être exécutés depuis main()
     *
     * @return void
     */
    public function main()
    {
        $nb_actions = $this->user->getNbActions();

        $pages = new \App\Services\Paginate($nb_actions, 10, 'page');
        $pagination = $pages->paginate();

        $this->setVariablesToView(array(
            'audit' => $this->user->getUsersAudit(array('limit' => $pagination['limit'], 'offset' => $pagination['offset'])),
            'nb_actions' => $nb_actions,
            'nb_pages' => $pagination['nb_pages'],
            'current_page' => $pagination['current_page'],
        ));
    }
}
