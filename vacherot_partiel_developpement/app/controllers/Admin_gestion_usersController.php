<?php

/**
 * Class HomeController
 *
 * Fichier de vue inclue: home
 * Ce fichier étends la class AbstractPageSystem où sont accessibles:
 * Les super globales: $this->get, $this->get, $this->get, $this->get, $this->get
 *
 * @author : Kévin Vacherot
 *
 */

namespace App\Controllers;

class Admin_gestion_usersController extends \Core\System\AbstractPageSystem
{
    private $user;
    /**
     * __Constructeur: C'est dans le constructeur que nous définissons les informations de la page courante
     * @return void
     */
    public function __construct()
    {
        $this->setPageView('admin_gestion_users.twig');
        $this->setPageInfos(array(
            'page_title' => 'Gérer les utilisateurs'
        ));
        $this->user = new \App\Models\User();
    }

    /**
     * Méthode appelée systématiquement par le controller courant depuis la classe PageSystem
     * C'est depuis cette méthode que seront gérées:
     * L'ensemble des données revoyées vers la vue (select de données par exemples)
     * L'ensemble des traitement générés depuis la vue (envoi de formulaire par exemple)
     *
     * Les traitements pourront évidemment être effectués depuis d'autres méthodes de la classe : traiter des post et retourner des données
     * Mais ils devront être exécutés depuis main()
     *
     * @return void
     */
    public function main()
    {
        if(isset($this->get['suppr_user'])){
            $this->deleteUser($this->get['suppr_user']);
        }

        if(isset($this->get['status']) && isset($this->get['user_id'])){
            $this->changeUserStatus($this->get['user_id'], $this->get['status']);
        }

        $nb_users = $this->user->getNbUsers();

        $pages = new \App\Services\Paginate($nb_users, 10, 'page');
        $pagination = $pages->paginate();

        $this->setVariablesToView(array(
            'users' => $this->user->getUsers(array('limit' => $pagination['limit'], 'offset' => $pagination['offset'])),
            'nb_users' => $nb_users,
            'nb_pages' => $pagination['nb_pages'],
            'current_page' => $pagination['current_page'],
        ));
    }

    public function changeUserStatus($user_id, $new_status){
        if($this->user->changeUserStatus($user_id, $new_status)){
            $this->redirect('admin_gestion_users', array('user_status_changed' => 'success'));
        } else {
            $this->redirect('admin_gestion_users', array('user_status_changed' => 'failed'));
        }
    }


    public function deleteUser($user_id)
    {
        if($this->user->deleteUser($user_id)){
            $this->redirect('admin_gestion_users', array('delete_user' => 'success'));
        } else {
            $this->redirect('admin_gestion_users', array('delete_user' => 'failed'));
        }
    }
}
