<?php

/**
 * Class HomeController
 *
 * Fichier de vue inclue: home
 * Ce fichier étends la class AbstractPageSystem où sont accessibles:
 * Les super globales: $this->get, $this->get, $this->get, $this->get, $this->get
 *
 * @author : Kévin Vacherot
 *
 */

namespace App\Controllers;

class Admin_loginController extends \Core\System\AbstractPageSystem
{
    private $user;
    /**
     * __Constructeur: C'est dans le constructeur que nous définissons les informations de la page courante
     * @return void
     */
    public function __construct()
    {
        $this->setPageView('admin_login.twig');
        $this->setPageInfos(array(
            'page_title' => 'Connexion espace gestion user'
        ));
        $this->user = new \App\Models\User();
    }

    /**
     * Méthode appelée systématiquement par le controller courant depuis la classe PageSystem
     * C'est depuis cette méthode que seront gérées:
     * L'ensemble des données revoyées vers la vue (select de données par exemples)
     * L'ensemble des traitement générés depuis la vue (envoi de formulaire par exemple)
     *
     * Les traitements pourront évidemment être effectués depuis d'autres méthodes de la classe : traiter des post et retourner des données
     * Mais ils devront être exécutés depuis main()
     *
     * @return void
     */
    public function main()
    {
        if(isset($this->post['connect'])){
            $this->createAdminSession($this->post);
        }
        $this->setVariablesToView(array(
            'hello_world' => 'hello_world!',
        ));
    }


    private function  createAdminSession($form_post){
        $mail = $form_post['mail'];
        $password = md5($form_post['password']);

        $users_datas = $this->user->getAdminUser($mail, $password);

        if($users_datas !== false){
            $_SESSION['admin'] = $users_datas;
            $this->redirect('admin_gestion_users');
        } else {
            $this->redirect('admin_login', array(
                'error' => 'unknown_user',
            ));
        }
    }
}
