<?php

/**
 * Class Error404Controller
 *
 * Fichier de vue inclue: 404
 * Ce fichier étends la class AbstractPageSystem où sont accessibles:
 * Les super globales: $this->get, $this->get, $this->get, $this->get, $this->get
 *
 * @author : Kévin Vacherot
 * @author : Kévin Siow
 *
 */

namespace App\Controllers;

class Error404Controller extends \Core\System\AbstractPageSystem
{
    /**
     * __Constructeur: C'est dans le constructeur que nous définissons les informations de la page courante
     * @return void
     */
    public function __construct()
    {
        $this->setPageView('404.twig');
        $this->setPageInfos(array(
            'page_title' => '404 - Page not found'
        ));
    }

    public function main()
    {

    }
}
