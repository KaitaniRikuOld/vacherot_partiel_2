<?php

/**
 * Class HomeController
 *
 * Fichier de vue inclue: index
 * Ce fichier étends la class AbstractPageSystem où sont accessibles:
 * Les super globales: $this->get, $this->get, $this->get, $this->get, $this->get
 *
 * @author : Kévin Vacherot
 * @author : Kévin Siow
 *
 */

namespace App\Controllers;

class IndexController extends \Core\System\AbstractPageSystem
{
    /**
     * __Constructeur: C'est dans le constructeur que nous définissons les informations de la page courante
     * @return void
     */
    public function __construct()
    {
        $this->setPageView('index.twig');
        $this->setPageInfos(array(
            'page_title' => 'Welcome'
        ));
    }

    /**
     * Méthode appelée systématiquement par le controller courant depuis la classe PageSystem
     * C'est depuis cette méthode que seront gérées:
     * L'ensemble des données revoyées vers la vue (select de données par exemples)
     * L'ensemble des traitement générés depuis la vue (envoi de formulaire par exemple)
     *
     * Les traitements pourront évidemment être effectués depuis d'autres méthodes de la classe : traiter des post et retourner des données
     * Mais ils devront être exécutés depuis main()
     *
     * @return void
     */
    public function main()
    {
        $this->setVariablesToView(array(
            'hello_world' => 'hello_world!',
        ));
    }
}
