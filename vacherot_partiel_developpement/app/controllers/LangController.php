<?php

/**
 * Class LangController
 * Ce contrôleur charge le bon fichier de langue et set le cookie de langue
 * Ce fichier étends la class AbstractPageSystem où sont accessibles:
 * Les super globales: $this->get, $this->get, $this->get, $this->get, $this->get
 *
 * @author : Kévin Vacherot
 *
 */

namespace App\Controllers;

class LangController extends \Core\System\AbstractPageSystem
{
    /**
     * __Constructeur: C'est dans le constructeur que nous définissons les informations de la page courante
     * @return void
     */
    public function __construct()
    {
        $this->setPageView(false);
    }

    public function main()
    {
        $lang = isset($_GET['lang']) && ($_GET['lang'] === 'en' || $_GET['lang'] === 'fr') ? $_GET['lang'] : 'en';
        if(!isset($_COOKIE['lang']) || $_COOKIE['lang'] !== $lang){
            setcookie('lang', $lang, time()+3600*24*7);
        }
        header('location: '.$_SERVER['HTTP_REFERER']);
    }
}