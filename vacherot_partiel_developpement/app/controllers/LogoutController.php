<?php

/**
 * Class LangController
 * Ce contrôleur charge le bon fichier de langue et set le cookie de langue
 * Ce fichier étends la class AbstractPageSystem où sont accessibles:
 * Les super globales: $this->get, $this->get, $this->get, $this->get, $this->get
 *
 * @author : Kévin Vacherot
 *
 */

namespace App\Controllers;

class LogoutController extends \Core\System\AbstractPageSystem
{
    /**
     * __Constructeur: C'est dans le constructeur que nous définissons les informations de la page courante
     * @return void
     */
    public function __construct()
    {
        $this->setPageView(false);
    }

    public function main()
    {
        session_destroy();
    	session_unset();
    	setcookie('remember', '', time()-1000);
        $this->redirect('admin_login', array(
            'deconnected' => 'true',
        ));
    }
}
