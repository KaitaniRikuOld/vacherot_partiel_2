<?php

/**
 * Class User
 *
 * Cette classe, comme toutes les class contenues dans le dossier Models, hérite de la Class Database\Models
 * Cette classe hérite ainsi de l'objet PDO, mais également des requêtes pré-faites de la classe Models
 *
 * @author : Kévin Vacherot
 *
 */

namespace App\Models;
use \PDO;

class User extends \Core\Database\Models
{
    /**
     * __Constructeur: Appel automatiquement le constructeur de Models afin d'établir la connexion
     * avec la bdd et de récupérer l'objet PDO
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Méthode permettant d'effectuer la connexion de l'utilisateur :
     * Si le couple mail / password,  est présent une seule fois dans la base de donnée
     * Et que le compte est activé, on retourne ses infos utilisateurs sous forme de tableau
     * @param   string  $mail  Mail de l'utilisateur
     * @param   string  $password  Password de l'utilisateur
     * @return bool / string / array infos de l'utilisateur, false si pas de user enregistré avec ce mail/mdp, "multiple" si plusieurs users enregistrés
     */
    public function getAdminUser($mail, $password)
    {
        $query = "SELECT
                        users.user_id
                        , users.user_pseudo

                      FROM aaa_users AS users

                      WHERE TRUE
                      AND users.user_mail = :user_mail
                      AND users.user_password = :user_password
                      AND users.user_status = 1";

        $values = array(
            ':user_mail' => array($mail, PDO::PARAM_STR),
            ':user_password' => array($password, PDO::PARAM_STR),
        );

        $user_exist = $this->existingUniqResult($query, $values);

        return $user_exist ? $this->executeWithBindedValues($query, $values, 'select', 'one') : false;
    }

    public function getUsers($params = array())
    {
        $query = "SELECT
                            view.user_id AS id
                          , view.user_pseudo AS pseudo
                          , view.user_mail AS mail
                          , view.user_password AS password
                          , view.user_status AS status
                          , view.category_id AS category_id
                          , view.category_name AS category_name

                          FROM aaa_users_and_categories AS view

                          WHERE TRUE";

        $values = array();

        if(isset($params['user_id'])){
            $query .= " AND view.user_id = :user_id ";
            $values[':user_id'] = array($params['user_id'], PDO::PARAM_INT);
            $results = $this->executeWithBindedValues($query, $values, true, 'one');
        } else {
            $query .= " ORDER BY view.user_id DESC ";

            if(!empty($params)){
                if(isset($params['limit']) && isset($params['offset'])){
                    $query .=" LIMIT :limit, :offset";
                    $values[':limit'] = array($params['limit'], PDO::PARAM_INT);
                    $values[':offset'] = array($params['offset'], PDO::PARAM_INT);
                }
                $results = $this->executeWithBindedValues($query, $values, true, 'all');
            } else {
                $results = $this->fetchResults($query, 'all');
            }
        }

        return $results;
    }

    public function getNbUsers()
    {
        return $this->countResults('aaa_users', 'user_id')[0];
    }

    public function insertUser($user_pseudo, $user_mail, $user_password, $category_id)
    {
        $query = $this->buildInsertQuery(
            'aaa_users',
            array(
                'user_pseudo' => ':user_pseudo',
                'user_mail' => ':user_mail',
                'user_password' => ':user_password',
                'user_status' => 0,
                'category_id' => ':category_id',
            )
        );

        return $this->executeWithBindedValues(
            $query,
            array(
                ':user_pseudo' => array($user_pseudo, PDO::PARAM_STR),
                ':user_mail' => array($user_mail, PDO::PARAM_STR),
                ':user_password' => array($user_password, PDO::PARAM_STR),
                ':category_id' => array($category_id, PDO::PARAM_INT),
            )
        );
    }

    public function updateUser($user_id, $user_pseudo, $user_mail, $category_id)
    {
        $query = $this->buildUpdateQuery(
            'aaa_users',
            array(
                'user_pseudo' => ':user_pseudo',
                'user_mail' => ':user_mail',
                'category_id' => ':category_id',
            ),
            array('user_id' => ':user_id')
        );

        $array_binded_values = array(
            ':user_pseudo' => array($user_pseudo, PDO::PARAM_STR),
            ':user_mail' => array($user_mail, PDO::PARAM_STR),
            ':category_id' => array($category_id, PDO::PARAM_INT),
            ':user_id' => array($user_id, PDO::PARAM_INT),
        );

        return $this->executeWithBindedValues($query, $array_binded_values);
    }


    public function changeUserStatus($user_id, $status)
    {
        $query = $this->buildUpdateQuery('aaa_users', array('user_status' => ':status'), array('user_id' => ':user_id'));

        return $this->executeWithBindedValues(
            $query,
            array(
                ':user_id' => array($user_id, PDO::PARAM_INT),
                ':status' => array($status, PDO::PARAM_INT),
            )
        );
    }

    public function deleteUser($user_id)
    {
        $query = $this->buildDeleteQuery('aaa_users', array('user_id' => ':user_id'));

        return $this->executeWithBindedValues(
            $query,
            array(':user_id' => array($user_id, PDO::PARAM_INT))
        );
    }


    public function getNbActions()
    {
        return $this->countResults('aaa_action_on_users_audit', 'audit_id')[0];
    }


    public function getUsersAudit($params = array())
    {
        $query = "SELECT
                            audit.audit_id AS audit_id
                          , audit.audit_action AS audit_action
                          , audit.audit_action_date AS audit_action_date
                          , audit.audit_old_pseudo AS audit_old_pseudo
                          , audit.audit_old_mail AS audit_old_mail
                          , audit.audit_old_password AS audit_old_password
                          , categories.category_name

                          FROM aaa_action_on_users_audit AS audit

                          LEFT JOIN aaa_categories AS categories
                          ON categories.category_id = audit.audit_old_category

                          WHERE TRUE";

        $values = array();

        if(isset($params['user_id'])){
            $query .= " AND audit.audit_id = :audit_id ";
            $values[':audit_id'] = array($params['audit_id'], PDO::PARAM_INT);
            $results = $this->executeWithBindedValues($query, $values, true, 'one');
        } else {
            $query .= " ORDER BY audit.audit_id DESC ";

            if(!empty($params)){
                if(isset($params['limit']) && isset($params['offset'])){
                    $query .=" LIMIT :limit, :offset";
                    $values[':limit'] = array($params['limit'], PDO::PARAM_INT);
                    $values[':offset'] = array($params['offset'], PDO::PARAM_INT);
                }
                $results = $this->executeWithBindedValues($query, $values, true, 'all');
            } else {
                $results = $this->fetchResults($query, 'all');
            }
        }

        return $results;
    }
}
