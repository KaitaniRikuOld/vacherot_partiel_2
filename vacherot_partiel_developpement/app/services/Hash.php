<?php

/**
 * Class Hash
 *
 * Cette class prévoit plusieurs méthodes static destinées à effectuer des contrôles de saisie
 *
 * @author : Kévin Vacherot
 * @author : Kévin Siow
 *
 */

 namespace App\Services;

class Hash
{
    public static function md5($string)
    {
        return md5($string);
    }
}
