<?php

/**
  * Class Paginate
  *
  * Composant permettant de gérer la pagination de résultats
  *
  * @author Kévin Vacherot
  * @author Kévin Siow
*/

namespace App\Services;

class Paginate
{
    public $nb_articles;
    public $nb_article_page;
    public $get_param;

    public function __construct($nb_articles, $nb_article_page, $get_param)
    {
        $this->nb_articles = $nb_articles;
        $this->nb_article_page = $nb_article_page;
        $this->get_param = $get_param;
    }

    public function paginate()
    {
    	$nb_pages = ceil($this->nb_articles/$this->nb_article_page);

    	if(isset($_GET[$this->get_param])){
    		$page = $_GET[$this->get_param];

    		if($page > $nb_pages){
    			$page = $nb_pages;
    		} elseif($page <= 0) {
    			$page = 1;
    		}
    	} else {
    		$page = 1;
    	}

    	return array(
            'nb_pages' => $nb_pages,
            'current_page' => $page,
            'limit' => ($page-1) * $this->nb_article_page,
            'offset' => $this->nb_article_page
        );
    }


}
