<?php

/**
 * Class Session
 *
 * Cette classe gère la session
 *
 * @author : Kévin Vacherot
 * @author : Kévin Siow
 *
 */

namespace App\Services;

class Session
{
    /**
     * Méthode permettant l'attribution d'un nom à la session,  et une protection contre le vol de session
     * @param string  $name  Nom de la session
     * @return bool
     */
    public static function start($name = '')
	{
		session_name($name);
		session_start();

		$ip = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
		$securite = $ip . '_' . $_SERVER['HTTP_USER_AGENT'];

		if(!isset($_SESSION['integrity'])) {
			$_SESSION['integrity'] = $securite;
			return true;
		} else {
			if($_SESSION['integrity'] != $securite) {
				session_regenerate_id();
				$_SESSION = array();
				return false;
			} else {
				return true;
			}
		}
	}
}
