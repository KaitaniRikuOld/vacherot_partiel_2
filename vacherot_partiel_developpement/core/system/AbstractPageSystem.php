<?php

/**
 * Class AbstractSystem
 *
 *
 * @author : Kévin Vacherot
 * @author : Kévin Siow
 *
 */

namespace Core\System;

abstract class AbstractPageSystem
{
    /**
     * @var array  Equivalent de la super globale $_GET
     */
    protected $get;

    /**
     * @var array  Equivalent de la super globale $_POST
     */
    protected $post;

    /**
     * @var array  Equivalent de la super globale $_FILES
     */
    protected $files;

    /**
     * @var array  Equivalent de la super globale $_REQUEST
     */
    protected $request;

    /**
     * @var array  tableau contenant les informations de la page courante
     */
    protected $pageInfos;

    /**
     * @var string  Chemin du "fichier vue" affiché par le controller"
     */
    protected $pageView;

    /**
     * @var array  Tableau contenant les variables envoyées sur la pageView
     */
    protected $variablesToView;


    /**
     * Setter: Méthode utilisé pour Initialiser les propriétes $get, $post, $files, $request, $session
     * @param array  $get  Equivalent de la super globale $_GET
     * @param array  $post  Equivalent de la super globale $_POST
     * @param array  $files  Equivalent de la super globale $_FILES
     * @param array  $request  Equivalent de la super globale $_REQUEST
     * @return void
     */
    protected function setGlobals($get, $post, $files, $request)
    {
        $this->get = $get;
        $this->post = $post;
        $this->files = $files;
        $this->request = $request;
    }


    /**
     * Setter: Méthode utilisé pour Initialiser la propriété $pageInfos
     * @param array  Tableau contenant les infos de la page courante
     * @return void
     */
    public function setPageInfos($pageInfos)
    {
        $this->pageInfos = $pageInfos;
    }


    /**
     * Setter: Méthode utilisé pour Initialiser la propriété $pageView
     * @param string  $pageView  Chemin du "fichier vue" affiché par le controller"
     * @return void
     */
    protected function setPageView($pageView)
    {
        $this->pageView = $pageView;
    }


    /**
     * Setter: Méthode utilisé pour Initialiser la propriété $variablesToView
     * @param array  Tableau contenant les variables envoyées sur la pageView
     * @return void
     */
    protected function setVariablesToView($arrayVariables)
    {
        $this->variablesToView = $arrayVariables;
    }


    /**
     * Getter: Méthode qui retourne Tableau contenant les infos de la page courante
     * @return array Tableau contenant les infos de la page courante
     */
    public function getPageInfos()
    {
        return $this->pageInfos;
    }


    /**
     * Getter: Méthode qui retourne le tableau de variables envoyé à la vue
     * @return array Tableau contenant les variables envoyées sur la pageView
     */
    public function getVariablesToView()
    {
        return $this->variablesToView;
    }


    /**
     * Getter: Méthode qui retourne le chemin du "fichier vue" affiché par le controller"
     * @return string Chemin du "fichier vue" affiché par le controller"
     */
    public function getPageView()
    {
        return $this->pageView;
    }


    /**
     * Méthode permettant de faire une redirection
     * @param string  $page  Page vers laquelle on souhaite rediriger le user (ex: home pour la page p=home)
     * @param array  $params  Tableau les paramètres additionnels que l'on veut ajouter dans l'url
     * @return void
     */
    public function redirect($page, $params = false)
    {
        $url_params = '';

        if($params){
            foreach($params as $key => $value){
                $url_params .= '&' . $key . '=' . $value;
            }
        }

        header('location: index.php?p=' . $page . $url_params);
    }
}
