<?php

/**
 * Class PageSystem
 *
 *
 * @author : Kévin Vacherot
 *
 */

namespace Core\System;

class PageSystem extends AbstractPageSystem
{
    /**
     * @var array  Equivalent de la super globale $_GET
     */
    protected $get;

    /**
     * @var array  Equivalent de la super globale $_POST
     */
    protected $post;

    /**
     * @var array  Equivalent de la super globale $_FILES
     */
    protected $files;

    /**
     * @var array  Equivalent de la super globale $_REQUEST
     */
    protected $request;

    /**
     * @var array  retour de la requête ajax
     */
    public $ajaxReturn = array();


    /**
     * __Constructeur:
     * Le constructeur initialise les propriétés de la classe
     * Et déclenche la méthode setPageController, permettant de lancer le controller correspondant à la page courante
     * @param array  $get  Equivalent de la super globale $_GET
     * @param array  $post  Equivalent de la super globale $_POST
     * @param array  $files  Equivalent de la super globale $_FILES
     * @param array  $request  Equivalent de la super globale $_REQUEST
     * @return void
     */
    public function __construct($get, $post, $files, $request, $ajax)
    {
        $this->get = $get;
        $this->post = $post;
        $this->files = $files;
        $this->request = $request;

        if(!$ajax){
            $page = isset($this->get['p']) ? $this->get['p'] : 'index';
            $this->setPageController($page);
        } else {
            $module = isset($this->get['module']) && !empty($this->get['module']) ? $this->get['module'] : false;
            $action = isset($this->get['action']) && !empty($this->get['action']) ? $this->get['action'] : false;

            if(!$module){
                $this->ajaxReturn['error']['no_module_called'] = true;
            }

            if(!$action){
                $this->ajaxReturn['error']['no_method_called'] = true;
            }

            $module && $action ? $this->callAjaxMethod($module, $action) : null;
        }
    }


    /**
     * Méthode appelée par le contrôleur
     * Elle permet d'instancier le controller correspondant à la page courante
     * @param string  $page  Nom de la page actuelle correspondant à $this->get['p']
     * @return object   Retourne l'instance du controller activé selon la page
     */
    private function setPageController($page)
    {
        $controllerName = ucfirst($page) . 'Controller';
        $pageController = 'app\controllers\\' . $controllerName;
        $pathController = str_replace('\\', '/', $pageController . '.php');

        $controller = is_file($pathController) ? new $pageController() : new \App\Controllers\Error404Controller();

        $controller->setGlobals($this->get, $this->post, $this->files, $this->request);
        $controller->main();
        $this->setVariablesToView($controller->getVariablesToView());
        $this->setpageView($controller->getpageView());
        $this->setPageInfos($controller->getPageInfos());

        return $controller;
    }



    /**
     * Méthode appelée par le contrôleur
     * Elle permet de déclencher le bon controller et la bonne méthode, suivant les params de la requête Ajax
     * @param string  $module  Nom du controller à instancier correspondant à $this->get['module']
     * @param string  $action  Nom de la méthode à exécuter correspondant à $this->get['action']
     * @return void
     */
    private function callAjaxMethod($module, $action)
    {
        $controllerName = 'Ajax' . ucfirst($module) . 'Controller';
        $moduleController = '\app\controllers\\' . $controllerName;
        $pathModuleController = str_replace('\\', '/', ROOT . $moduleController . '.php');

        $controller = is_file($pathModuleController) ? new $moduleController() : array('module_no_exist' => true);
        is_file($pathModuleController) ? $controller->setGlobals($this->get, $this->post, $this->files, $this->request) : null;
        $this->ajaxReturn = method_exists($controller, $action) ? $controller->$action() : array('method_no_exist' => true);
    }
}
