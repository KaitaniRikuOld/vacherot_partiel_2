<?php

/**
 * Class System
 *
 * Cette classe est un singleton destiné à démarrer l'architecture
 *
 * @author : Kévin Vacherot
 *
 */

namespace Core\System;

class System
{
    /**
    * @var object  Instance unique de la Class MainController (Singleton)
    */
    private static $instance;

    /**
    * @var array  Tableau contenant la configuration du serveur à adopter (dev / test / prod)
    */
    private $environmentConfiguration;

    /**
    * @var array  Tableau contenant le fichier langue (en ou fr)
    */
    private $lang;

    /**
    * @var object  Instance de la Class PageSystem
    */
    private $pageSystem;

    /**
    * @var array  Tableau contenant les information nécessaire à la connexion à la base de données, et à la création de l'object PDO
    */
    private $dbConfiguration;

    /**
    * @var array  Tableau contenant les configurations pour TWIG
    */
    private $twigConfiguration;

    /**
    * @var array  Tableau contenant les configurations pour les templates TWIG
    */
    private $twigTemplateConfiguration;

    /**
    * @var array  Tableau contenant les fichier CSS à charger pour chaque page
    */
    private $stylesheetConfiguration;

    /**
    * @var array  Tableau contenant les fichier less à compiler en fichier css, puis à charger pour chaque page
    */
    private $lessConfiguration;

    /**
    * @var array  Tableau contenant les fichier JS à charger pour chaque page
    */
    private $jsFileConfiguration;

    /**
    * @var array  Tableau contenant la configuration bootstrap
    */
    private $bootstrapConfiguration;

    /**
    * @var array  Tableau contenant les pages protégé par un system de backoffice, et la page de destination de la redirection
    */
    private $adminConfiguration;

    /**
    * @var bool  Indique si ajax
    */
    private static $ajax = false;


    /**
     * __Constructeur:
     * L'opérateur de portée appliqué au constructeur est "private"
     * Cela permet de s'assurer que la class ne puisse être instanciée que via la méthode Start()
     * Cette class a en effet la particularité d'être un singleton
     *
     * Le constructeur initialise les propriétés de la classe
     * @return void
     */
    private function __construct()
    {
        if(!self::$ajax){
            $this->twigConfiguration = $this->getTwigConfiguration();
            $this->twigTemplateConfiguration = $this->getTwigTemplateConfiguration();
            $this->stylesheetConfiguration = $this->getStylesheetConfiguration();
            $this->lessConfiguration = $this->getLessConfiguration();
            $this->jsFileConfiguration = $this->getJsFileConfiguration();
            $this->bootstrapConfiguration = $this->getBootstrapConfiguration();
            $this->adminConfiguration = $this->getAdminConfiguration();
            //$this->stylesheetConfiguration['use']['create_files'] ? $this->createFiles('www/assets/css/', $this->stylesheetConfiguration) : null;
            //$this->lessConfiguration['use']['create_files'] ? $this->createFiles('www/assets/less/', $this->lessConfiguration) : null;
            //$this->jsFileConfiguration['use']['create_files'] ? $this->createFiles('www/assets/js/', $this->jsFileConfiguration) : null;
            if($this->lessConfiguration['use']['use_less'] !== false){
                $this->compileLess();
            }
        }

        $this->environmentConfiguration = $this->getEnvironmentConfiguration();
    }


    /**
     * Méthode appelée par start()
     * La classe MainController applique le principe de fonctionnement du singleton
     * la méthode load() vérifie alors si la classe est déjà instanciée, et ne créer une nouvelle instance que dans le cas contraire
     * @return object Retourne l'instance de la classe.
     */
    private static function load()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    /**
     * La Méthode start() instancie la classe
     * Etablie l'environnement de développement
     * Déclenche la classe gérant le rootage et le system de page
     * Génère le système de vues avec TWIG
     * @return void
     */
    public static function start($ajax = null)
    {
        if(is_null($ajax)){
            $return = self::load()
                ->setEnvironment()
                ->setLang()
                ->loadPageSystem()
                ->setAdmin()
                ->loadTwig();
        } else {
            self::$ajax = true;
            $return = self::load()
                ->setEnvironment()
                ->loadPageSystem()
                ->returnAjaxResult();
        }

        return $return;
    }


    private function returnAjaxResult()
    {
        return $this->pageSystem->ajaxReturn;
    }


    /**
     * Méthode appelée par start()
     * Méthode permettant de configurer l'environnement de développement (dev, test, prod) en fonction
     * du fichier de configuration config-environment.twig
     * @return object Instance de la class courante
     */
    private function setEnvironment()
    {
        if($this->environmentConfiguration['server'] === 'dev'){
            define('DEBUG', true);
            define('ENV', 'DEV');
        } elseif($this->environmentConfiguration['server'] === 'test'){
            define('DEBUG', false);
            define('ENV', 'TEST');
        } elseif($this->environmentConfiguration['server'] === 'prod') {
            define('DEBUG', false);
            define('ENV', 'PROD');
        }

        if(defined('DEBUG') && DEBUG){
            ini_set('display_errors', 1);
            error_reporting(E_ALL);
        } else {
            ini_set('display_errors', 0);
            error_reporting(0);
        }

        return $this;
    }


    /**
     * Méthode appelée par start()
     * Méthode permettant de définir le fichier de lang à appliquer
     * @return object Instance de la class courante
     */
    private function setLang()
    {
        if(isset($_COOKIE['lang'])){
            $url = 'lang/' . $_COOKIE['lang'] . '.php';
        } else {
            $url = 'lang/en.php';
        }

        $this->lang = is_file($url) ? require $url : null;

        return $this;
    }


    /**
     * Méthode appelée par start()
     * Initialise la propriété $pageSystem qui devient une instance de la class PageSystem
     * La classe PageSystem gère en principalement le chargement des controllers
     * @return object   Instance de la class courante
     */
    private function loadPageSystem()
    {
        $this->pageSystem = new PageSystem(
            $_GET
            , $_POST
            , $_FILES
            , $_REQUEST
            , self::$ajax
        );

        return $this;
    }


    /**
     * Méthode appelée par le constructeur()
     * Cette méthode compile les fichiers less en fichier css depuis le répertoire www/assets/less/, dans www/assets/css/
     * @return void
     */
    private function compileLess()
    {
        require "app/vendors/less/lessc.inc.php";

        $less = $less = new \lessc;
        $lessFolderFileList = scandir('www/assets/less/');

        foreach ($lessFolderFileList as $key => $lessFile) {
            if($lessFile != '.' && $lessFile != '..' ){
                $filename = strtolower(substr(strstr($lessFile, '.', true), 0));
                $lessFileLocation = 'www/assets/less/' . $filename . '.less';
                $cssFileDestination = 'www/assets/css/' . $filename . '.css';

                $less->compileFile($lessFileLocation, $cssFileDestination);
            }
        }
    }


    /**
     * Méthode appelée par start()
     * Cette méthode est utiliser pour initialiser twig afin de l'utiliser pour le system de vue
     * Déclenche la méthode renderArrayTwig() chargée de retourner le tableau de variables à envoyer à TWIG
     * @return void
     */
    private function loadTwig()
    {
        require "app/vendors/twig/autoload.php";
        if($this->pageSystem->getPageView() !== false){
            $loader = new \Twig_Loader_Filesystem($this->twigConfiguration['Core']['folder']);
            $twig = new \Twig_Environment($loader, array(
                'cache' => false,
                'debug' => defined('DEBUG') && DEBUG ? true : false,
            ));
            $twig->addExtension(new \Twig_Extension_Debug());
            $template = $twig->loadTemplate($this->defineTwigTemplate());
            $arrayRender = $this->renderArrayTwig();

            $template->display($arrayRender);
        }

        return $this;
    }


    private function defineTwigTemplate()
    {
        $page = isset($_GET['p']) ? $_GET['p'] : 'home';

        $template = $this->twigTemplateConfiguration['Default']['template'];
        foreach($this->twigTemplateConfiguration as $key => $value){
            if($key !== 'Default'){
                if(in_array($page, $this->twigTemplateConfiguration[$key]['pages'])){
                    $template = $this->twigTemplateConfiguration[$key]['template'];
                }
            }
        }

        return $template;
    }


    /**
     * Méthode appelée par loadTwig()
     *
     * Cette méthode est Chargée de retourner le tableau de variables à envoyer à TWIG
     * @return array   tableau des variables à envoyer à TWIG
     */
    private function renderArrayTwig()
    {
        $page = isset($_GET['p']) ? $_GET['p'] : 'home';

        $arrayRender = array(
            $this->twigConfiguration['Vars']['globals']['get'] => $_GET,
            $this->twigConfiguration['Vars']['globals']['post'] => $_POST,
            $this->twigConfiguration['Vars']['globals']['files'] => $_FILES,
            $this->twigConfiguration['Vars']['globals']['request'] => $_REQUEST,
            $this->twigConfiguration['Vars']['globals']['session'] => $_SESSION,
            $this->twigConfiguration['Vars']['globals']['cookie'] => $_COOKIE,
            $this->twigConfiguration['Vars']['globals']['server'] => $_SERVER,
            $this->twigConfiguration['Vars']['controller']['project_var'] => $this->pageSystem->getVariablesToView(),
            $this->twigConfiguration['Vars']['controller']['page_infos'] => $this->pageSystem->getpageInfos(),
            $this->twigConfiguration['Vars']['controller']['current_view'] => $this->twigConfiguration['Core']['views_location'] . $this->pageSystem->getPageView(),
            $this->twigConfiguration['Vars']['files']['js'] => $this->jsFileConfiguration['use']['link_files'] ? $this->renderJsFiles($page) : array(),
            $this->twigConfiguration['Vars']['files']['css'] => $this->stylesheetConfiguration['use']['link_files'] ? $this->renderStylesheetFiles($page) : array(),
            $this->twigConfiguration['Vars']['files']['less'] => $this->lessConfiguration['use']['link_files'] ? $this->renderLessFiles($page) : array(),
            $this->twigConfiguration['Vars']['files']['bootstrap_files'] => $this->renderBootstrapFiles(),
            'current_url' =>"http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
            'content' => $this->lang,

        );
    

        if(defined('DEBUG') && DEBUG){
            $arrayRender['debug_tool'] = true;
            $arrayRender['debug_global_vars'] = array_merge(
                array('get' => $_GET),
                array('post' => $_POST),
                array('files' => $_FILES),
                array('request' => $_REQUEST),
                array('session' => $_SESSION),
                array('cookie' => $_COOKIE),
                array('server' => $_SERVER)
            );
        }

        return $arrayRender;
    }


    private function setAdmin(){
        $adminConfiguration = $this->adminConfiguration;

        $page = isset($_GET['p']) ? $_GET['p'] : 'home';

        if(!isset($_SESSION['admin'])){
            if(in_array($page, $adminConfiguration['back_office']['pages_admin'])){
                header('location: index.php?p=' . $adminConfiguration['back_office']['redirect']);
            }
        } else {
            if ($page === $adminConfiguration['back_office']['redirect']){
                header('location: index.php?p=' . $adminConfiguration['back_office']['redirect_once_login']);
            }
        }

        return $this;
    }


    /**
     * Méthode appelée par le constructeur()
     * Elle est utilisée afin de créer les fichiers js, css et/ou less, dynamiquement depuis les fichiers de config
     * @param string  $pathFileLocation  Chemin du repertoire où générer le fichier
     * @param array  $arrayFilesConfiguration  Tableau contenant l'ensemble des fichiers js, css ou less
     * @return void
     */
    private function createFiles($pathFileLocation, $arrayFilesConfiguration)
    {
        $files = array();

        foreach ($arrayFilesConfiguration['files'] as $key => $value) {
            if(!empty($value)){
                foreach ($value as $k => $v) {
                    $files[] = $v;
                }
            }
        }

        foreach ($files as $key => $value) {
            !is_file($pathFileLocation . $value) ? fopen($pathFileLocation . $value, 'a') : null;
        }
    }


    /**
     * Méthode appelée par le constructeur
     * Elle est utilisée afin de parcourir le fichier YAML admin/pages.yaml afin d'en extraire un tableau
     * Ce tableau contient les pages protégé par un system de backoffice, et la page de destination de la redirection
     * Ce tableau est ensuite utilisé pour initialiser la propriété $adminConfiguration
     * @return array   Tableau contenant les pages protégé par un system de backoffice, et la page de destination de la redirection
     */
    private function getAdminConfiguration()
    {
        return \Core\Configuration::parseYamlFile('admin/pages');
    }


    /**
     * Méthode appelée par le constructeur
     * Elle est utilisée afin de parcourir le fichier YAML config_environment.yaml afin d'en extraire un tableau
     * Ce tableau contient la configuration du serveur à adopter (dev / test / prod)
     * Ce tableau est ensuite utilisé pour initialiser la propriété $environmentConfiguration
     * @return array   Tableau contenant la configuration du serveur à adopter (dev / test / prod)
     */
    private function getEnvironmentConfiguration()
    {
        return \Core\Configuration::parseYamlFile('environment/config_environment');
    }


    /**
     * Méthode appelée par le constructeur
     * Elle est utilisée afin de parcourir le fichier YAML config_twig.yaml afin d'en extraire un tableau
     * Ce tableau contient les configurations pour TWIG
     * Ce tableau est ensuite utilisé pour initialiser la propriété $twigConfiguration
     * @return array   Tableau contenant les configurations pour TWIG
     */
    private function getTwigConfiguration()
    {
        return \Core\Configuration::parseYamlFile('twig/config_twig');
    }


    /**
     * Méthode appelée par le constructeur
     * Elle est utilisée afin de parcourir le fichier YAML config_templates.yaml afin d'en extraire un tableau
     * Ce tableau contient les configurations pour les templates TWIG
     * Ce tableau est ensuite utilisé pour initialiser la propriété $twigTemplateConfiguration
     * @return array   Tableau contenant les configurations pour TWIG
     */
    private function getTwigTemplateConfiguration()
    {
        return \Core\Configuration::parseYamlFile('twig/config_templates');
    }


    /**
     * Méthode appelée par le constructeur
     * Elle est utilisée afin de parcourir le fichier YAML config_stylesheet.yaml afin d'en extraire un tableau
     * Ce tableau contient les fichiers css à charger pour chaque page
     * Ce tableau est ensuite utilisé pour initialiser la propriété $stylesheetConfiguration
     * @return array   Tableau contenant les fichiers css à charger pour chaque page
     */
    private function getStylesheetConfiguration()
    {
        return \Core\Configuration::parseYamlFile('design_files/config_stylesheet');
    }


    /**
     * Méthode appelée par le constructeur
     * Elle est utilisée afin de parcourir le fichier YAML config_less.yaml afin d'en extraire un tableau
     * Ce tableau contient les fichiers less à compiler en fichier.css, puis à charger pour chaque page
     * Ce tableau est ensuite utilisé pour initialiser la propriété $lessConfiguration
     * @return array   Tableau contenant les fichiers less à compiler en fichier.css, puis à charger pour chaque page
     */
    private function getLessConfiguration()
    {
        return \Core\Configuration::parseYamlFile('design_files/config_less');
    }


    /**
     * Méthode appelée par le constructeur
     * Elle est utilisée afin de parcourir le fichier YAML config_jsfile.yaml afin d'en extraire un tableau
     * Ce tableau contient les fichiers js à charger pour chaque page
     * Ce tableau est ensuite utilisé pour initialiser la propriété $jsFileConfiguration
     * @return array   Tableau contenant les fichiers js à charger pour chaque page
     */
    private function getJsFileConfiguration()
    {
        return \Core\Configuration::parseYamlFile('design_files/config_jsfile');
    }


    /**
     * Méthode appelée par le constructeur
     * Elle est utilisée afin de parcourir le fichier YAML config_bootstrap.yaml afin d'en extraire un tableau
     * Ce tableau contient les fichiers js et css à charger dans le cas ou on souhaite utiliser bootstrap
     * Ce tableau est ensuite utilisé pour initialiser la propriété $bootstrapConfiguration
     * @return array   Tableau contenant les fichiers js et css à charger dans le cas ou on souhaite utiliser bootstrap
     */
    private function getBootstrapConfiguration()
    {
        return \Core\Configuration::parseYamlFile('design_files/config_bootstrap');
    }


    /**
     * Méthode appelée par renderArrayTwig()
     * Elle est utilisée afin de transmettre à la page courante, les fichiers js correspondants
     * @param string  $currentPage  Nom de la page courante
     * @return array   Tableau des fichiers js correspondant à la page courante
     */
    private function renderJsFiles($currentPage)
    {
        $JsFilesForAll = isset($this->jsFileConfiguration['files']['All']) && !empty($this->jsFileConfiguration['files']['All']) ? $this->jsFileConfiguration['files']['All'] : array();
        $JsFilesForPage = isset($this->jsFileConfiguration['files'][ucfirst($currentPage)]) && !empty($this->jsFileConfiguration['files'][ucfirst($currentPage)]) ? $this->jsFileConfiguration['files'][ucfirst($currentPage)] : array();

        return array_merge($JsFilesForAll, $JsFilesForPage);
    }


    /**
     * Méthode appelée par renderArrayTwig()
     * Elle est utilisée afin de transmettre à la page courante, les fichiers css correspondants
     * @param string  $currentPage  Nom de la page courante
     * @return array   Tableau des fichiers css correspondant à la page courante
     */
    private function renderStylesheetFiles($currentPage)
    {
        $cssFilesForAll = isset($this->stylesheetConfiguration['files']['All']) && !empty($this->stylesheetConfiguration['files']['All']) ? $this->stylesheetConfiguration['files']['All'] : array();
        $cssFilesForPage = isset($this->stylesheetConfiguration['files'][ucfirst($currentPage)]) && !empty($this->stylesheetConfiguration['files'][ucfirst($currentPage)]) ? $this->stylesheetConfiguration['files'][ucfirst($currentPage)] : array();

        return array_merge($cssFilesForAll, $cssFilesForPage);
    }


    /**
     * Méthode appelée par renderArrayTwig()
     * Elle est utilisée afin de transmettre à la page courante, les fichiers css (issue de la compilatoin par less) correspondants
     * @param string  $currentPage  Nom de la page courante
     * @return array   Tableau des fichiers less correspondant à la page courante
     */
    private function renderLessFiles($currentPage)
    {
        $lessFilesForAll = isset($this->lessConfiguration['files']['All']) && !empty($this->lessConfiguration['files']['All']) ? $this->lessConfiguration['files']['All'] : array();
        $lessFilesForPage = isset($this->lessConfiguration['files'][ucfirst($currentPage)]) && !empty($this->lessConfiguration['files'][ucfirst($currentPage)]) ? $this->lessConfiguration['files'][ucfirst($currentPage)] : array();

        return $this->convertIntoCss(array_merge($lessFilesForAll, $lessFilesForPage));
    }


    /**
     * Méthode appelée par renderLessFiles()
     * Elle est utilisée afin de convertir les nom de fichier .less en fichier .css, afin de linker
     * les fichiers css compilés dans les pages "html"
     * @param array  $arrayLessFiles  Tableau des fichiers css (issue de la compilatoin par less) correspondant à la page courante
     * @return array  Tableau des fichiers css (issue de la compilatoin par less) correspondant à la page courante
     */
    private function convertIntoCss($arrayLessFiles)
    {
        $arrayConvertedFiles = array();

        foreach ($arrayLessFiles as $key => $lessFile) {
            $arrayConvertedFiles[] = strtolower(substr(strstr($lessFile, '.', true), 0)) . '.css';
        }

        return $arrayConvertedFiles;
    }


    /**
     * Méthode appelée par renderArrayTwig()
     * Elle est utilisée afin de transmettre à la page courante, les fichiers js et css de bootstrap si souhaités
     * @return array   Tableau des fichiers js et css de bootstrap si souhaités
     */
    private function renderBootstrapFiles()
    {
        return $this->bootstrapConfiguration['bootstrap'];
    }
}
