<?php

/**
 *
 * Framework MVC
 *
 * @author Kevin Vacherot <kevin.vacherot@eemi.com>
 * @author Kevin Siow <kevin.siow@eemi.com>
 *
 */

 /**
  * Inclusion de l'autoloader
  */
require_once 'core/Autoloader.php';

/**
 * Inclusion des constantes du projet
 */
require_once 'configuration/constantes.php';

/**
 * Lancement de l'autoloader pour charger toutes les classes du projet
 */
Core\Autoloader::register();

/**
 * Démarrage de la sesison
 */
App\Services\Session::start(SESSION_NAME);

/**
 * Démarrage du system
 */
Core\System\System::start();
