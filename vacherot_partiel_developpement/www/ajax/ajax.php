<?php

/**
 * Inclusion de l'autoloader
 */
require_once '../../core/Autoloader.php';

/**
 * Inclusion des constantes du projet
 */
require_once '../../configuration/constantes.php';

/**
 * Lancement de l'autoloader pour charger toutes les classes du projet
 */
Core\Autoloader::register();

/**
 * Démarrage de la sesison
 */
App\Services\Session::start('my_session');

/**
 * JSON encode du retour
 */
//header('Content-type: application/json; charset=UTF-8');

echo json_encode(Core\System\System::start('ajax'));
