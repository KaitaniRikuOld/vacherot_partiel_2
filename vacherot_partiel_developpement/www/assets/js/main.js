$(document).ready(function(){
    $('#contact').on('submit', function(e){
        e.preventDefault();

        var datas = $(this).serialize();
        ajaxSendMessages(datas);
    })

    function ajaxSendMessages(datas) {
        $.ajax({
            url: 'www/ajax/ajax.php?module=contact&action=sendMessage',
            type: 'POST',
            datatype: 'json',
            data: datas,
            beforeSend: function () {
                //   $('.loader').show();
            },
            success: function(data) {
              console.log(data);
            },
        });
    }
});
